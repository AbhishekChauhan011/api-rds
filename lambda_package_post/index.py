#Post Function 
import pymysql
import json
import os

endpoint  = os.environ['endpoint']
username = 'Admin'
password = 'Chirag123'
database_name = os.environ['databaseName']

connection = pymysql.connect(host=endpoint, user=username, passwd=password, db=database_name)

def lambda_handler(event, context):
    item_count = 0

    data = json.loads(event['body'])

    id = data['Id']
    name = data['Name']
    category = data['Category']
    year = data['Year']

    with connection.cursor() as cur:
        cur.execute("insert into Movies (Id, Name, Category, Year) values(%s, %s, %s, %s)",(id, name, category, year))
        connection.commit()
        cur.execute("select * from Movies")
        for row in cur:
            item_count += 1

    connection.commit()

    responseObject = {}
    responseObject["isBase64Encoded"] = False
    responseObject["statusCode"] = 200
    responseObject["headers"] = {}
    responseObject["headers"]["Content-Type"] = "application/json"
    responseObject["headers"]["Access-Control-Allow-Origin"] = "*"
    responseObject["body"] = "Suceessfully Uploaded items into the rds mysql table"

    return responseObject
