#Put Function 
import pymysql
import json
import os

endpoint  = os.environ['endpoint']
username = 'Admin'
password = 'Chirag123'
database_name = os.environ['databaseName']

connection = pymysql.connect(host=endpoint, user=username, passwd=password, db=database_name)

def lambda_handler(event, context):
    data = json.loads(event['body'])

    id = data['Id']
    name = data['Name']

    with connection.cursor() as cur:
        cur.execute('update Movies set Name=%s where Id=%s', (name, id))
        connection.commit()

    connection.commit()

    responseObject = {}
    responseObject["isBase64Encoded"] = False
    responseObject["statusCode"] = 200
    responseObject["headers"] = {}
    responseObject["headers"]["Content-Type"] = "application/json"
    responseObject["headers"]["Access-Control-Allow-Origin"] = "*"
    responseObject["body"] = "Suceessfully Updated new items into the rds mysql table"

    return responseObject
