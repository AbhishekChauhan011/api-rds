#Get Function 
import pymysql
import json
import os

endpoint  = os.environ['endpoint']
username = 'Admin'
password = 'Chirag123'
database_name = os.environ['databaseName']

connection = pymysql.connect(host=endpoint, user=username, passwd=password, db=database_name)

def lambda_handler(event, context):
    data = []
    with connection.cursor() as cur:
        cur.execute("select * from Movies")
        for row in cur:
            data.append(row)
            
    connection.commit()
    
    responseObject = {}
    responseObject["isBase64Encoded"] = False
    responseObject["statusCode"] = 200
    responseObject["headers"] = {}
    responseObject["headers"]["Content-Type"] = "application/json"
    responseObject["headers"]["Access-Control-Allow-Origin"] = "*"
    responseObject["body"] = json.dumps(data)

    return responseObject
