#Create Schema Function 
import pymysql
import json
import os
import sys

endpoint  = os.environ['endpoint']
username = 'Admin'
password = 'Chirag123'
database_name = os.environ['databaseName']

connection = pymysql.connect(host=endpoint, user=username, passwd=password, db=database_name)

def lambda_handler(event, context):
    with connection.cursor() as cur:
        cur.execute("CREATE TABLE if not exists Movies ( Id int NOT NULL, Name varchar(255) NOT NULL, Category varchar(255) NOT NULL, Year int NOT NULL, PRIMARY KEY (Id))")
        return